#include <stdio.h>
#include "inteiro.h"

inline static enum sinal sinal_reverso(enum sinal sinal);

inline static unsigned short character_para_int(char algarismo);

inline static char int_para_character(unsigned short numero);

inline static void inserir_algarismo_int_em_inteiro_quociente(struct inteiro *numero, unsigned short algarismo);

static void inserir_n_quantidade_de_zeros_em_inteiro(struct inteiro *numero, size_t n);

static enum comparacao_cadeia_numerica inteiro_comparacao(struct inteiro *numero_A, struct inteiro *numero_B);

static struct inteiro ZERO_INTEIRO =
        {
                .quociente =
                        {
                                .algarismos = "0",
                                .tamanho_cadeia = 1,
                                .tamanho_cadeia_alocada = 0},
                .sinal = POSITIVO};

static struct inteiro UM_INTEIRO =
        {
                .quociente =
                        {
                                .algarismos = "1",
                                .tamanho_cadeia = 1,
                                .tamanho_cadeia_alocada = 0},
                .sinal = POSITIVO};

struct inteiro criar_inteiro_vazio() {
    struct inteiro numero;

    numero.quociente = criar_cadeia_numerica_vazia();
    numero.resto = criar_cadeia_numerica_vazia();
    numero.sinal = POSITIVO;

    return numero;
}

struct inteiro criar_inteiro(const char *string, enum sinal sinal, size_t tamanho) {
    struct inteiro numero;
    size_t indice_nao_zero = retornar_indice_de_inicio_nao_zero_de_string(string, tamanho);
    numero.quociente = criar_cadeia_numerica(string + indice_nao_zero, tamanho - indice_nao_zero);
    numero.resto = criar_cadeia_numerica_vazia();
    numero.sinal = sinal;

    return numero;
}

void deletar_inteiro(struct inteiro *numero) {
    if (numero != NULL) {
        deletar_cadeia_numerica(&numero->quociente);
        deletar_cadeia_numerica(&numero->resto);
    }
}

void limpar_inteiro(struct inteiro *numero) {
    if (numero != NULL) {
        limpar_cadeia_numerica(&numero->quociente);
        limpar_cadeia_numerica(&numero->resto);
    }
}

bool inteiro_vazio(struct inteiro *numero) {
    return numero != NULL && cadeia_numerica_vazia(&numero->quociente) && cadeia_numerica_vazia(&numero->resto);
}

bool inteiro_maior_que(struct inteiro *numero_possivelmente_maior, struct inteiro *numero_possivelmente_menor) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_possivelmente_maior,
                                                                    numero_possivelmente_menor);

    if (comparacao != FALHA) {
        if (numero_possivelmente_maior->sinal == numero_possivelmente_menor->sinal) {
            if (numero_possivelmente_maior->sinal == POSITIVO) {
                return comparacao == MAIOR;
            } else {
                return comparacao == MENOR;
            }
        } else {
            return numero_possivelmente_maior->sinal == POSITIVO;
        }
    } else {
        return false;
    }
}

bool
inteiro_em_modulo_maior_que(struct inteiro *numero_possivelmente_maior, struct inteiro *numero_possivelmente_menor) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_possivelmente_maior,
                                                                    numero_possivelmente_menor);

    return comparacao == MAIOR;
}

bool inteiro_maior_igual_que(struct inteiro *numero_possivelmente_maior, struct inteiro *numero_possivelmente_menor) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_possivelmente_maior,
                                                                    numero_possivelmente_menor);

    if (comparacao != FALHA) {
        if (numero_possivelmente_maior->sinal == numero_possivelmente_menor->sinal) {
            if (numero_possivelmente_maior->sinal == POSITIVO) {
                return comparacao == MAIOR || comparacao == IGUAL;
            } else {
                return comparacao == MENOR || comparacao == IGUAL;
            }
        } else {
            return numero_possivelmente_maior->sinal == POSITIVO;
        }
    } else {
        return false;
    }
}

bool inteiro_em_modulo_maior_igual_que(struct inteiro *numero_possivelmente_maior,
                                       struct inteiro *numero_possivelmente_menor) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_possivelmente_maior,
                                                                    numero_possivelmente_menor);

    return comparacao == MAIOR || comparacao == IGUAL;
}

bool inteiro_menor_que(struct inteiro *numero_possivelmente_menor, struct inteiro *numero_possivelmente_maior) {
    return inteiro_maior_igual_que(numero_possivelmente_maior, numero_possivelmente_menor);
}

bool
inteiro_em_modulo_menor_que(struct inteiro *numero_possivelmente_menor, struct inteiro *numero_possivelmente_maior) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_possivelmente_menor,
                                                                    numero_possivelmente_maior);

    return comparacao == MENOR;
}

bool inteiro_menor_igual_que(struct inteiro *numero_possivelmente_menor, struct inteiro *numero_possivelmente_maior) {
    return inteiro_maior_que(numero_possivelmente_maior, numero_possivelmente_menor);
}

bool inteiro_em_modulo_menor_igual_que(struct inteiro *numero_possivelmente_menor,
                                       struct inteiro *numero_possivelmente_maior) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_possivelmente_menor,
                                                                    numero_possivelmente_maior);

    return comparacao == MENOR || comparacao == IGUAL;
}

bool inteiro_igual_que(struct inteiro *numero_possivelmente_igual_A, struct inteiro *numero_possivelmente_igual_B) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_possivelmente_igual_A,
                                                                    numero_possivelmente_igual_B);
    return comparacao == IGUAL && numero_possivelmente_igual_A->sinal == numero_possivelmente_igual_B->sinal;
}

bool inteiro_em_modulo_igual_que(struct inteiro *numero_possivelmente_igual_A,
                                 struct inteiro *numero_possivelmente_igual_B) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_possivelmente_igual_A,
                                                                    numero_possivelmente_igual_B);
    return comparacao == IGUAL;
}

bool inteiro_diferente_que(struct inteiro *numero_A, struct inteiro *numero_B) {
    return !inteiro_igual_que(numero_A, numero_B);
}

bool inteiro_em_modulo_diferente_que(struct inteiro *numero_A, struct inteiro *numero_B) {
    return !inteiro_em_modulo_igual_que(numero_A, numero_B);
}

enum comparacao_inteiro
comparando_inteiros(struct inteiro *numero_A, struct inteiro *numero_B) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_A, numero_B);

    if (comparacao != FALHA) {
        if (numero_A->sinal == numero_B->sinal) {
            if (comparacao == IGUAL) {
                return IGUAL_INTEIRO;
            } else if ((numero_A->sinal == POSITIVO && comparacao == MAIOR) ||
                       (numero_A->sinal == NEGATIVO && comparacao == MENOR)) {
                return MAIOR_INTEIRO;
            }
        } else if (numero_A->sinal == POSITIVO) {
            return MAIOR_INTEIRO;
        }

        return MENOR_INTEIRO;
    } else {
        return FALHA_INTEIRO;
    }
}

enum comparacao_inteiro
comparando_inteiros_em_modulo(struct inteiro *numero_A, struct inteiro *numero_B) {
    enum comparacao_cadeia_numerica comparacao = inteiro_comparacao(numero_A, numero_B);
    switch (comparacao) {
        case MAIOR:
            return MAIOR_EM_MODULO_INTEIRO;
        case IGUAL:
            return IGUAL_EM_MODULO_INTEIRO;
        case MENOR:
            return MENOR_EM_MODULO_INTEIRO;
        default:
            return FALHA_INTEIRO;
    }
}

struct inteiro somar_dois_inteiros(struct inteiro *numero_A, struct inteiro *numero_B) {
    struct inteiro resultado = criar_inteiro_vazio();

    if (numero_A != NULL && numero_B != NULL &&
        !inteiro_vazio(numero_A) && !inteiro_vazio(numero_B)) {
        struct inteiro *maior_inteiro;
        struct inteiro *menor_inteiro;

        if (inteiro_em_modulo_maior_igual_que(numero_A, numero_B)) {
            maior_inteiro = numero_A;
            menor_inteiro = numero_B;
        } else {
            maior_inteiro = numero_B;
            menor_inteiro = numero_A;
        }

        resultado.sinal = maior_inteiro->sinal;

        if (maior_inteiro->sinal != menor_inteiro->sinal) {
            menor_inteiro->sinal = sinal_reverso(menor_inteiro->sinal);
            resultado = subtrair_dois_inteiros(maior_inteiro, menor_inteiro);
            menor_inteiro->sinal = sinal_reverso(menor_inteiro->sinal);
        } else {
            size_t indice;

            unsigned short valor_parcial_maior_inteiro;
            unsigned short valor_parcial_menor_inteiro;
            unsigned short valor_parcial_final;
            unsigned short quociente_parcial;

            quociente_parcial = 0;

            for (indice = 0; indice < maior_inteiro->quociente.tamanho_cadeia; indice++) {
                valor_parcial_maior_inteiro = character_para_int(
                        maior_inteiro->quociente.algarismos[maior_inteiro->quociente.tamanho_cadeia - 1 - indice]);

                if (indice < menor_inteiro->quociente.tamanho_cadeia) {
                    valor_parcial_menor_inteiro = character_para_int(
                            menor_inteiro->quociente.algarismos[menor_inteiro->quociente.tamanho_cadeia - 1 - indice]);
                } else {
                    valor_parcial_menor_inteiro = 0;
                }

                valor_parcial_final = valor_parcial_maior_inteiro + valor_parcial_menor_inteiro + quociente_parcial;
                quociente_parcial = valor_parcial_final / (unsigned short) 10;
                valor_parcial_final = valor_parcial_final % (unsigned short) 10;

                inserir_algarismo_int_em_inteiro_quociente(&resultado, valor_parcial_final);
            }

            if (quociente_parcial > 0) {
                inserir_algarismo_int_em_inteiro_quociente(&resultado, quociente_parcial);
            }

            reverter_cadeia_numerica(&resultado.quociente);
            resultado.sinal = maior_inteiro->sinal;
        }
    }

    return resultado;
}

struct inteiro subtrair_dois_inteiros(struct inteiro *numero_A, struct inteiro *numero_B) {
    struct inteiro resultado = criar_inteiro_vazio();

    if (numero_A != NULL && numero_B != NULL &&
        !inteiro_vazio(numero_A) && !inteiro_vazio(numero_B)) {
        struct inteiro *maior_inteiro;
        struct inteiro *menor_inteiro;

        const enum comparacao_inteiro comparacao = comparando_inteiros_em_modulo(numero_A, numero_B);

        if (comparacao == IGUAL_EM_MODULO_INTEIRO) {
            resultado = somar_dois_inteiros(&ZERO_INTEIRO, &ZERO_INTEIRO);
        } else {
            if (comparacao == MAIOR_EM_MODULO_INTEIRO) {
                maior_inteiro = numero_A;
                menor_inteiro = numero_B;
                resultado.sinal = maior_inteiro->sinal;
            } else {
                maior_inteiro = numero_B;
                menor_inteiro = numero_A;
                resultado.sinal = (menor_inteiro->sinal == NEGATIVO && menor_inteiro->sinal == maior_inteiro->sinal)
                                  ? POSITIVO : NEGATIVO;
            }

            if (maior_inteiro->sinal != menor_inteiro->sinal) {
                menor_inteiro->sinal = sinal_reverso(menor_inteiro->sinal);
                resultado = somar_dois_inteiros(maior_inteiro, menor_inteiro);
                menor_inteiro->sinal = sinal_reverso(menor_inteiro->sinal);
                resultado.sinal = numero_A->sinal;
            } else {
                size_t indice;

                short valor_parcial_maior_inteiro;
                short valor_parcial_menor_inteiro;
                short valor_parcial_final;
                short quociente_parcial;

                quociente_parcial = 0;

                for (indice = 0; indice < maior_inteiro->quociente.tamanho_cadeia; indice++) {
                    valor_parcial_maior_inteiro = character_para_int(
                            maior_inteiro->quociente.algarismos[maior_inteiro->quociente.tamanho_cadeia - 1 - indice]);

                    if (indice < menor_inteiro->quociente.tamanho_cadeia) {
                        valor_parcial_menor_inteiro = character_para_int(
                                menor_inteiro->quociente.algarismos[menor_inteiro->quociente.tamanho_cadeia - 1 -
                                                                    indice]);
                    } else {
                        valor_parcial_menor_inteiro = 0;
                    }

                    if (valor_parcial_maior_inteiro == valor_parcial_menor_inteiro && quociente_parcial > 0) {
                        quociente_parcial = 9;
                    } else if (valor_parcial_maior_inteiro < valor_parcial_menor_inteiro) {
                        quociente_parcial = (short) (quociente_parcial > 0 ? 9 : 10);
                    } else quociente_parcial = (short) (quociente_parcial > 0 ? -1 : 0);

                    valor_parcial_final =
                            (quociente_parcial + valor_parcial_maior_inteiro) - valor_parcial_menor_inteiro;

                    if ((indice + 1) < maior_inteiro->quociente.tamanho_cadeia || valor_parcial_final > 0)
                        inserir_algarismo_int_em_inteiro_quociente(&resultado, (unsigned short) valor_parcial_final);

                }

                reverter_cadeia_numerica(&resultado.quociente);
            }
        }
    }

    return resultado;
}

struct inteiro multiplicar_dois_inteiros(struct inteiro *numero_A, struct inteiro *numero_B) {
    struct inteiro resultado = criar_inteiro_vazio();

    if (numero_A != NULL && numero_B != NULL &&
        !inteiro_vazio(numero_A) && !inteiro_vazio(numero_B)) {
        struct inteiro *maior_inteiro;
        struct inteiro *menor_inteiro;
        struct inteiro parcial;
        struct inteiro resultado_parcial;

        size_t indice_menor, indice_maior;

        unsigned short valor_parcial_maior_inteiro;
        unsigned short valor_parcial_menor_inteiro;
        unsigned short valor_parcial_final;
        unsigned short quociente_parcial;

        if (inteiro_em_modulo_maior_igual_que(numero_A, numero_B)) {
            maior_inteiro = numero_A;
            menor_inteiro = numero_B;
        } else {
            maior_inteiro = numero_B;
            menor_inteiro = numero_A;
        }

        if (inteiro_em_modulo_igual_que(menor_inteiro, &UM_INTEIRO) == true) {
            resultado = somar_dois_inteiros(maior_inteiro, &ZERO_INTEIRO);
        } else if (inteiro_em_modulo_igual_que(menor_inteiro, &ZERO_INTEIRO) == true) {
            inserir_algarismo_int_em_inteiro_quociente(&resultado, 0);
        } else {

            inserir_algarismo_int_em_inteiro_quociente(&resultado, 0);

            parcial = criar_inteiro_vazio();

            for (indice_menor = 0; indice_menor < menor_inteiro->quociente.tamanho_cadeia; indice_menor++) {
                valor_parcial_menor_inteiro = character_para_int(
                        menor_inteiro->quociente.algarismos[menor_inteiro->quociente.tamanho_cadeia - 1 -
                                                            indice_menor]);
                quociente_parcial = 0;
                for (indice_maior = 0; indice_maior < maior_inteiro->quociente.tamanho_cadeia; indice_maior++) {
                    valor_parcial_maior_inteiro = character_para_int(
                            maior_inteiro->quociente.algarismos[maior_inteiro->quociente.tamanho_cadeia - 1 -
                                                                indice_maior]);
                    valor_parcial_final = valor_parcial_menor_inteiro * valor_parcial_maior_inteiro + quociente_parcial;
                    quociente_parcial = valor_parcial_final / (unsigned short) 10;
                    valor_parcial_final = valor_parcial_final % (unsigned short) 10;

                    inserir_algarismo_int_em_inteiro_quociente(&parcial, valor_parcial_final);
                }

                if (quociente_parcial > 0) {
                    inserir_algarismo_int_em_inteiro_quociente(&parcial, quociente_parcial);
                }

                reverter_cadeia_numerica(&parcial.quociente);
                inserir_n_quantidade_de_zeros_em_inteiro(&parcial, indice_menor);
                resultado_parcial = somar_dois_inteiros(&resultado, &parcial);

                limpar_inteiro(&parcial);

                deletar_inteiro(&resultado);
                resultado = resultado_parcial;
            }

            deletar_inteiro(&parcial);
        }

        resultado.sinal = maior_inteiro->sinal == menor_inteiro->sinal ? POSITIVO : NEGATIVO;
    }

    return resultado;
}

static enum comparacao_cadeia_numerica
inteiro_comparacao(struct inteiro *numero_A, struct inteiro *numero_B) {
    if (numero_A != NULL && numero_B != NULL &&
        !inteiro_vazio(numero_A) && !inteiro_vazio(numero_B)) {
        return comparar_cadeias_numericas(&numero_A->quociente, &numero_B->quociente);
    }

    return FALHA;
}

inline static enum sinal sinal_reverso(enum sinal sinal) {
    return sinal == POSITIVO ? NEGATIVO : POSITIVO;
}

inline static unsigned short character_para_int(char algarismo) {
    return (unsigned short) (algarismo - '0');
}

inline static char int_para_character(unsigned short numero) {
    return (char) (numero + '0');
}

static void inserir_n_quantidade_de_zeros_em_inteiro(struct inteiro *numero, size_t n) {
    size_t indice;

    for (indice = 1; indice <= n; indice++) {
        inserir_algarismo_em_cadeia_numerica(&numero->quociente, '0');
    }
}

inline static void inserir_algarismo_int_em_inteiro_quociente(struct inteiro *numero, unsigned short algarismo) {
    inserir_algarismo_em_cadeia_numerica(&numero->quociente, int_para_character(algarismo));
}
