#ifndef H_INTEIRO_H
#define H_INTEIRO_H

#include "../memoria/cadeia_numerica.h"
#include "sinal.h"

/**
 * Enumeração Comparação inteiro, outro tipo para retorno de comparação de inteiros.
 */
enum comparacao_inteiro
{
    FALHA_INTEIRO,
    MAIOR_INTEIRO,
    MAIOR_EM_MODULO_INTEIRO,
    MENOR_INTEIRO,
    MENOR_EM_MODULO_INTEIRO,
    IGUAL_INTEIRO,
    IGUAL_EM_MODULO_INTEIRO,
};

/**
 * Estrutura básica para inteiro
 */
struct inteiro
{
    struct cadeia_numerica quociente;
    struct cadeia_numerica resto;
    enum sinal sinal;
};

/**
 * Cria inteiro sem inserir uma cadeia string inicial
 */
extern struct inteiro criar_inteiro_vazio();

/**
 * Cria inteiro tentando inserir uma string como cadeia de algarismo
 * Caso nao seja possível criar, retorna um inteiro vazio
 */
extern struct inteiro criar_inteiro(const char *string, enum sinal sinal, size_t tamanho);

/**
 * libera memória de quociente e resto se possível
 */
extern void deletar_inteiro(struct inteiro *numero);

/**
 * Limpar inteiro para reutilização
 */ 
extern void limpar_inteiro(struct inteiro *numero);

/**
 * verifica se inteiro não contém nenhum valor
 */
extern bool inteiro_vazio(struct inteiro *numero);

/**
 * Verifica se inteiro é maior
 */
extern bool
inteiro_maior_que(struct inteiro *numero_possivelmente_maior, struct inteiro *numero_possivelmente_menor);

/**
 * Verifica se em modulo inteiro é maior
 */
extern bool
inteiro_em_modulo_maior_que(struct inteiro * numero_possivelmente_maior, struct inteiro * numero_possivelmente_menor);

/**
 * Verifica se inteiro é maior ou igual
 */
extern bool
inteiro_maior_igual_que(struct inteiro *numero_possivelmente_maior, struct inteiro *numero_possivelmente_menor);

/**
 * Verifica se em modulo inteiro é maior ou igual
 */
extern bool
inteiro_em_modulo_maior_igual_que(struct inteiro * numero_possivelmente_maior, struct inteiro * numero_possivelmente_menor);

/**
 * Verificar se inteiro é menor
 */
extern bool
inteiro_menor_que(struct inteiro *numero_possivelmente_menor, struct inteiro *numero_possivelmente_maior);

/**
 * Verificar se em modulo inteiro é menor
 */
extern bool
inteiro_em_modulo_menor_que(struct inteiro *numero_possivelmente_menor, struct inteiro *numero_possivelmente_maior);

/**
 * Verificar se inteiro é menor ou igual
 */
extern bool
inteiro_menor_igual_que(struct inteiro *numero_possivelmente_menor, struct inteiro *numero_possivelmente_maior);

/**
 * Verificar se em modulo inteiro é menor ou igual
 */
extern bool
inteiro_em_modulo_menor_igual_que(struct inteiro *numero_possivelmente_menor, struct inteiro *numero_possivelmente_maior);

/**
 * Verificar se inteiros são iguais
 */
extern bool
inteiro_igual_que(struct inteiro *numero_possivelmente_igual_A, struct inteiro *numero_possivelmente_igual_B);

/**
 * Verificar se em modulo inteiros são iguais
 */
extern bool
inteiro_em_modulo_igual_que(struct inteiro *numero_possivelmente_igual_A, struct inteiro *numero_possivelmente_igual_B);

/**
 * Verificar se inteiros são diferentes
 */
extern bool
inteiro_diferente_que(struct inteiro *numero_A, struct inteiro *numero_B);

/**
 * Verificar se em módulo inteiros são diferentes
 */
extern bool
inteiro_em_modulo_diferente_que(struct inteiro *numero_A, struct inteiro *numero_B);

/**
 * Comparação inteiros e retorna tipo enumerado
 */
extern enum comparacao_inteiro
comparando_inteiros(struct inteiro *numero_A, struct inteiro *numero_B);

/**
 * Comparação em módulo de inteiros e retorna tipo enumerado
 */
extern enum comparacao_inteiro
comparando_inteiros_em_modulo(struct inteiro *numero_A, struct inteiro *numero_B);

/**
 * Somar dois inteiros e retornar valor resultante
 */
extern struct inteiro somar_dois_inteiros(struct inteiro *numero_A, struct inteiro *numero_B);

/**
 * Subtrair dois inteiros e retornar valor resultante
 */
extern struct inteiro subtrair_dois_inteiros(struct inteiro *numero_A, struct inteiro *numero_B);

/**
 * Multiplicar dois inteiros e retornar valor resultante
 */ 
extern struct inteiro multiplicar_dois_inteiros(struct inteiro * numero_A, struct inteiro * numero_B);

#endif
