#ifndef H_CADEIA_NUMERICA_H
#define H_CADEIA_NUMERICA_H

#include <stddef.h>
#include <stdbool.h>

/**
 * Estrutura básica para cadeia numerica
 */
struct cadeia_numerica
{
    char *algarismos;
    size_t tamanho_cadeia;
    size_t tamanho_cadeia_alocada;
};

/**
 * Enumeração base para lidar com comparação de cadeias
 */
enum comparacao_cadeia_numerica
{
    FALHA,
    IGUAL,
    MAIOR,
    MENOR
};

/**
 * Cria uma cadeia numerica com ponteiro nulo e size_t's com valores iniciais iguais a 0.
 */
extern struct cadeia_numerica
criar_cadeia_numerica_vazia();

/**
 * Criar uma cadeia numerica gerada a partir de uma cadeia de string numerica
 * Caso string não seja numerica, retorna cadeia numerica vazia.
 */
extern struct cadeia_numerica criar_cadeia_numerica(const char *string, size_t tamanho_cadeia);

/**
 * Libera memória de algarismo de cadeia númerica
 */
extern void deletar_cadeia_numerica(struct cadeia_numerica *cadeia);

/**
 * Limpa cadeia númerica e mantém memória já alocada
 */
extern void limpar_cadeia_numerica(struct cadeia_numerica *cadeia);

/**
 * Insere um novo algarismo em cadeia númerica.
 * Caso não é possível inserir, retorna -1
 * Caso contrário retorna tamanho da cadeia
 */
extern int inserir_algarismo_em_cadeia_numerica(struct cadeia_numerica *cadeia, char algarismo);

/**
 * Verifica se na cadeia numerica contém apenas (0123456789)
 */
extern bool cadeia_numerica_string_valida(const char *string, size_t tamanho_cadeia);

/**
 * Verifica se cadeia numerica é vazia
 */
extern bool cadeia_numerica_vazia(struct cadeia_numerica *cadeia);

/**
 * transformar uma cadeia númerica em seu espelho.
 * exemplo: "0123456789" -> "9876543210"
 */
extern void reverter_cadeia_numerica(struct cadeia_numerica *cadeia);

/**
 * Compara duas cadeias numericas e retorna tipo enumerado comparacao cadeia numerica
 */
extern enum comparacao_cadeia_numerica
comparar_cadeias_numericas(struct cadeia_numerica *cadeia_A, struct cadeia_numerica *cadeia_B);

/**
 * Retorna endereço a partir do primeiro algarismo não zero
 */ 
extern size_t retornar_indice_de_inicio_nao_zero_de_string(const char * string, size_t tamanho);

#endif