#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "cadeia_numerica.h"

static bool digito_numerico(char algarismo);


struct cadeia_numerica criar_cadeia_numerica_vazia()
{
    struct cadeia_numerica cadeia =
        {
            .algarismos = NULL,
            .tamanho_cadeia = 0,
            .tamanho_cadeia_alocada = 0};

    return cadeia;
}

struct cadeia_numerica criar_cadeia_numerica(const char *string, size_t tamanho_cadeia)
{
    struct cadeia_numerica cadeia;

    if (cadeia_numerica_string_valida(string, tamanho_cadeia))
    {
        cadeia.tamanho_cadeia_alocada = tamanho_cadeia + 1;
        cadeia.algarismos = (char *)malloc(cadeia.tamanho_cadeia_alocada * sizeof(char));
        if (cadeia.algarismos == NULL)
        {
            fprintf(stderr, "(%s,%d)Error: Não foi possível alocar memória\n", __FILE__, __LINE__);
            exit(-1);
        }
        cadeia.tamanho_cadeia = tamanho_cadeia;
        strncpy(cadeia.algarismos, string, tamanho_cadeia);
        cadeia.algarismos[tamanho_cadeia] = '\0';
    }
    else
    {
        cadeia = criar_cadeia_numerica_vazia();
    }

    return cadeia;
}

void deletar_cadeia_numerica(struct cadeia_numerica *cadeia)
{
    if (cadeia != NULL)
    {
        free(cadeia->algarismos);
    }
}

void limpar_cadeia_numerica(struct cadeia_numerica *cadeia)
{
    if (cadeia != NULL)
    {
        cadeia->tamanho_cadeia = 0;
    }
}

int inserir_algarismo_em_cadeia_numerica(struct cadeia_numerica *cadeia, char algarismo)
{
    if (cadeia != NULL && digito_numerico(algarismo))
    {
        if(cadeia->tamanho_cadeia_alocada == 0)
        {
            cadeia->tamanho_cadeia_alocada = 10;
            cadeia->algarismos = (char *)malloc(cadeia->tamanho_cadeia_alocada * sizeof(char));
            if (cadeia->algarismos == NULL)
            {
                fprintf(stderr, "(%s,%d)Error: Não foi possível alocar memória\n", __FILE__, __LINE__);
                exit(-1);
            }
        }
        else if ((cadeia->tamanho_cadeia + 1) == cadeia->tamanho_cadeia_alocada)
        {
            cadeia->tamanho_cadeia_alocada += cadeia->tamanho_cadeia_alocada;
            cadeia->algarismos = (char *)realloc(cadeia->algarismos, cadeia->tamanho_cadeia_alocada * sizeof(char));
            if (cadeia->algarismos == NULL)
            {
                fprintf(stderr, "(%s,%d)Error: Não foi possível alocar memória \n", __FILE__, __LINE__);
                exit(-1);
            }
        }
        cadeia->algarismos[cadeia->tamanho_cadeia] = algarismo;
        cadeia->tamanho_cadeia += 1;
        cadeia->algarismos[cadeia->tamanho_cadeia] = '\0';
        return cadeia->tamanho_cadeia;
    }
    else
    {
        return -1;
    }
}

bool cadeia_numerica_vazia(struct cadeia_numerica *cadeia)
{
    return (cadeia->algarismos == NULL) &&
           (cadeia->tamanho_cadeia == 0) &&
           (cadeia->tamanho_cadeia_alocada == 0);
}

bool cadeia_numerica_string_valida(const char *string, size_t tamanho_cadeia)
{
    size_t indice;

    for (indice = 0; indice < tamanho_cadeia; indice++)
    {
        if (!digito_numerico(string[indice]))
        {
            return false;
        }
    }

    return tamanho_cadeia > 0;
}

void reverter_cadeia_numerica(struct cadeia_numerica *cadeia)
{
    if (cadeia != NULL && !cadeia_numerica_vazia(cadeia))
    {
        size_t indice;
        char algarismo;
        for (indice = 0; indice < cadeia->tamanho_cadeia / 2; indice++)
        {
            algarismo = cadeia->algarismos[cadeia->tamanho_cadeia - 1 - indice];
            cadeia->algarismos[cadeia->tamanho_cadeia - 1 - indice] = cadeia->algarismos[indice];
            cadeia->algarismos[indice] = algarismo;
        }
    }
}

enum comparacao_cadeia_numerica comparar_cadeias_numericas(struct cadeia_numerica *cadeia_A, struct cadeia_numerica *cadeia_B)
{
    enum comparacao_cadeia_numerica comparacao = FALHA;

    if (cadeia_A != NULL && cadeia_B != NULL && !cadeia_numerica_vazia(cadeia_A) && !cadeia_numerica_vazia(cadeia_B))
    {
        if (cadeia_A->tamanho_cadeia > cadeia_B->tamanho_cadeia)
        {
            comparacao = MAIOR;
        }
        else if (cadeia_A->tamanho_cadeia < cadeia_B->tamanho_cadeia)
        {
            comparacao = MENOR;
        }
        else
        {
            int comp = strcmp(cadeia_A->algarismos, cadeia_B->algarismos);

            if (comp == 0)
            {
                comparacao = IGUAL;
            }
            else if (comp < 0)
            {
                comparacao = MENOR;
            }
            else
            {
                comparacao = MAIOR;
            }
        }
    }

    return comparacao;
}

size_t retornar_indice_de_inicio_nao_zero_de_string(const char * string, size_t tamanho)
{
    size_t indice = 0;

    if(string != NULL)
    {
        while((indice + 1) < tamanho && string[indice] == '0')
        {
            indice = indice + 1;
        }
    }
    
    return indice;
}

static bool digito_numerico(char algarismo)
{
    /**
     * Non-zero value if the character is a numeric character, zero otherwise.
     */
    return isdigit(algarismo) != 0;
}
