#include <string.h>
#include <assert.h>
#include "../src/inteiro/inteiro.h"

static void inteiro_comparacao_ambos_sinais_positivos_1_test()
{
    const char *cadeia_A = "48434382349131";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "18434382349131";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    assert(inteiro_maior_que(&numero_A, &numero_B) == true);
    assert(comparando_inteiros(&numero_A, &numero_B) == MAIOR_INTEIRO);
    assert(inteiro_maior_que(&numero_B, &numero_A) == false);
    assert(inteiro_em_modulo_maior_que(&numero_A, &numero_B) == true);
    assert(inteiro_maior_igual_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_maior_igual_que(&numero_A, &numero_B) == true);
    assert(inteiro_menor_que(&numero_B, &numero_A) == true);
    assert(comparando_inteiros(&numero_B, &numero_A) == MENOR_INTEIRO);
    assert(inteiro_em_modulo_menor_que(&numero_B, &numero_A) == true);
    assert(inteiro_menor_igual_que(&numero_B, &numero_A) == true);
    assert(inteiro_em_modulo_menor_igual_que(&numero_B, &numero_A) == true);
    assert(inteiro_diferente_que(&numero_B, &numero_A) == true);
    assert(inteiro_diferente_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_B, &numero_A) == true);
    assert(comparando_inteiros_em_modulo(&numero_A, &numero_B) == MAIOR_EM_MODULO_INTEIRO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
}


static void inteiro_comparacao_ambos_sinais_positivos_2_test()
{
    const char *cadeia_A = "2139123832179";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "48434382349131";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    assert(inteiro_maior_que(&numero_A, &numero_B) == false);
    assert(comparando_inteiros(&numero_A, &numero_B) == MENOR_INTEIRO);
    assert(inteiro_em_modulo_maior_que(&numero_A, &numero_B) == false);
    assert(inteiro_maior_igual_que(&numero_A, &numero_B) == false);
    assert(inteiro_em_modulo_maior_igual_que(&numero_A, &numero_B) == false);
    assert(inteiro_menor_que(&numero_B, &numero_A) == false);
    assert(inteiro_em_modulo_menor_que(&numero_B, &numero_A) == false);
    assert(inteiro_menor_igual_que(&numero_B, &numero_A) == false);
    assert(inteiro_em_modulo_menor_igual_que(&numero_B, &numero_A) == false);
    assert(inteiro_diferente_que(&numero_B, &numero_A) == true);
    assert(comparando_inteiros_em_modulo(&numero_B, &numero_A) == MAIOR_EM_MODULO_INTEIRO);
    assert(inteiro_diferente_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_A, &numero_B) == true);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
}

static void inteiro_comparacao_ambos_sinais_negativos_test()
{
    const char *cadeia_A = "48434382349131";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "18434382349131";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    assert(inteiro_maior_que(&numero_B, &numero_A) == true);
    assert(comparando_inteiros(&numero_A, &numero_B) == MENOR_INTEIRO);
    assert(comparando_inteiros_em_modulo(&numero_A, &numero_B) == MAIOR_EM_MODULO_INTEIRO);
    assert(comparando_inteiros_em_modulo(&numero_B, &numero_A) == MENOR_EM_MODULO_INTEIRO);
    assert(inteiro_em_modulo_maior_que(&numero_A, &numero_B) == true);
    assert(inteiro_maior_igual_que(&numero_B, &numero_A) == true);
    assert(inteiro_em_modulo_maior_igual_que(&numero_B, &numero_A) == false);
    assert(inteiro_menor_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_menor_que(&numero_A, &numero_B) == false);
    assert(inteiro_menor_igual_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_menor_igual_que(&numero_A, &numero_B) == false);
    assert(inteiro_diferente_que(&numero_B, &numero_A) == true);
    assert(inteiro_diferente_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_A, &numero_B) == true);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
}

static void inteiro_comparacao_ambos_iguais_sinais_positivo_test()
{
    const char *cadeia_A = "48434382349131";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "48434382349131";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    assert(inteiro_igual_que(&numero_A, &numero_B) == true);
    assert(comparando_inteiros(&numero_A, &numero_B) == IGUAL_INTEIRO);
    assert(comparando_inteiros_em_modulo(&numero_A, &numero_B) == IGUAL_EM_MODULO_INTEIRO);
    assert(inteiro_em_modulo_igual_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_A, &numero_B) == false);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
}

static void inteiro_comparacao_ambos_iguais_sinais_negativo_test()
{
    const char *cadeia_A = "1";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "1";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    assert(inteiro_igual_que(&numero_A, &numero_B) == true);
    assert(comparando_inteiros(&numero_A, &numero_B) == IGUAL_INTEIRO);
    assert(comparando_inteiros_em_modulo(&numero_A, &numero_B) == IGUAL_EM_MODULO_INTEIRO);
    assert(inteiro_em_modulo_igual_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_A, &numero_B) == false);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
}

static void inteiro_comparacao_sinais_distintos_test()
{
    const char *cadeia_A = "0";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "48434382349131";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    assert(inteiro_maior_que(&numero_A, &numero_B) == true);
    assert(comparando_inteiros(&numero_A, &numero_B) == MAIOR_INTEIRO);
    assert(inteiro_em_modulo_maior_que(&numero_A, &numero_B) == false);
    assert(inteiro_maior_igual_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_maior_igual_que(&numero_A, &numero_B) == false);
    assert(inteiro_menor_que(&numero_B, &numero_A) == true);
    assert(comparando_inteiros(&numero_B, &numero_A) == MENOR_INTEIRO);
    assert(inteiro_em_modulo_menor_que(&numero_B, &numero_A) == false);
    assert(inteiro_menor_igual_que(&numero_B, &numero_A) == true);
    assert(inteiro_em_modulo_menor_igual_que(&numero_A, &numero_B) == true);
    assert(inteiro_diferente_que(&numero_B, &numero_A) == true);
    assert(inteiro_diferente_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_A, &numero_B) == true);

    const char *cadeia_C = "48434382349131";
    size_t tamanho_C = strlen(cadeia_C);
    enum sinal sinal_C = POSITIVO;
    struct inteiro numero_C = criar_inteiro(cadeia_C, sinal_C, tamanho_C);

    assert(inteiro_maior_que(&numero_C, &numero_B) == true);
    assert(comparando_inteiros(&numero_C, &numero_B) == MAIOR_INTEIRO);
    assert(comparando_inteiros_em_modulo(&numero_A, &numero_B) == MENOR_EM_MODULO_INTEIRO);
    assert(inteiro_em_modulo_maior_que(&numero_C, &numero_B) == false);
    assert(inteiro_maior_igual_que(&numero_C, &numero_B) == true);
    assert(inteiro_em_modulo_maior_igual_que(&numero_C, &numero_B) == true);
    assert(inteiro_menor_que(&numero_B, &numero_C) == true);
    assert(inteiro_em_modulo_menor_que(&numero_B, &numero_A) == false);
    assert(inteiro_menor_igual_que(&numero_B, &numero_C) == true);
    assert(inteiro_em_modulo_menor_igual_que(&numero_B, &numero_C) == true);
    assert(inteiro_diferente_que(&numero_B, &numero_C) == true);
    assert(inteiro_diferente_que(&numero_C, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_C, &numero_B) == false);
    assert(comparando_inteiros_em_modulo(&numero_C, &numero_B) == IGUAL_EM_MODULO_INTEIRO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&numero_C);
}

static void inteiro_comparacao_ambos_iguais_sinais_distintos_test()
{
    const char *cadeia_A = "1";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "1";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    assert(inteiro_diferente_que(&numero_A, &numero_B) == true);
    assert(inteiro_diferente_que(&numero_B, &numero_A) == true);
    assert(inteiro_em_modulo_igual_que(&numero_A, &numero_B) == true);
    assert(inteiro_em_modulo_diferente_que(&numero_A, &numero_B) == false);
    assert(comparando_inteiros(&numero_B, &numero_A) == MAIOR_INTEIRO);
    assert(comparando_inteiros_em_modulo(&numero_A, &numero_B) == IGUAL_EM_MODULO_INTEIRO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
}

int main(int argc, char const *argv[])
{
    inteiro_comparacao_ambos_sinais_positivos_1_test();
    inteiro_comparacao_ambos_sinais_positivos_2_test();
    inteiro_comparacao_ambos_sinais_negativos_test();
    inteiro_comparacao_ambos_iguais_sinais_positivo_test();
    inteiro_comparacao_ambos_iguais_sinais_negativo_test();
    inteiro_comparacao_sinais_distintos_test();
    inteiro_comparacao_ambos_iguais_sinais_distintos_test();
    return 0;
}
