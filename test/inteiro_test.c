#include <assert.h>
#include <string.h>
#include "../src/inteiro/inteiro.h"

static void criar_inteiro_vazio_test()
{
    struct inteiro numero_A = criar_inteiro_vazio();
    assert(inteiro_vazio(&numero_A) == true);
    deletar_inteiro(&numero_A);
}

static void criar_inteiro_test()
{
    const char *cadeia_A = "2348432843284324324236592912312913231290731209371231731";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal, tamanho_A);
    assert(numero_A.sinal == sinal);
    assert(strcmp(numero_A.quociente.algarismos, cadeia_A) == 0);
    assert(strncmp(numero_A.quociente.algarismos, cadeia_A, tamanho_A) == 0);
    assert(inteiro_vazio(&numero_A) == false);
    deletar_inteiro(&numero_A);

    //tentar criar inteiro com string invalida
    const char *cadeia_B = "2828234hhuh3832h23";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);
    assert(inteiro_vazio(&numero_B) == true);
    deletar_inteiro(&numero_B);
}

static void somar_dois_inteiros_1_test()
{
    const char *cadeia_A = "48434382349131";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "2139123832179";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "50573506181310") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_2_test()
{
    const char *cadeia_A = "2139123832179";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "48434382349131";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "50573506181310") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_3_test()
{
    const char *cadeia_A = "2139123832179";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "48434382349131";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "50573506181310") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_4_test()
{
    const char *cadeia_A = "3";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "5";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "8") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_5_test()
{
    const char *cadeia_A = "5";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "3";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "8") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_6_test()
{
    const char *cadeia_A = "0";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "0";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "0") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_7_test()
{
    const char *cadeia_A = "4328432483243283244711201331232173121632461022484512454542504154510543745";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "0";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "4328432483243283244711201331232173121632461022484512454542504154510543745") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_8_test()
{
    const char *cadeia_A = "4328432483243283244711201331232173121632461022484512454542504154510543745";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "9237327312971207141612194050000000000751165014757563737583921083745";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "4328441720570596215918342943426223121632461773649527212106241738431627490") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_9_test()
{
    const char *cadeia_A = "9";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "8";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "17") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void somar_dois_inteiros_test()
{
    somar_dois_inteiros_1_test();
    somar_dois_inteiros_2_test();
    somar_dois_inteiros_3_test();
    somar_dois_inteiros_4_test();
    somar_dois_inteiros_5_test();
    somar_dois_inteiros_6_test();
    somar_dois_inteiros_7_test();
    somar_dois_inteiros_8_test();
    somar_dois_inteiros_9_test();
}

static void multiplicar_dois_inteiros_1_test()
{
    const char *cadeia_A = "175";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "48";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = multiplicar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "8400") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void multiplicar_dois_inteiros_2_test()
{
    const char *cadeia_A = "4328432483243283244711201331232173121632461022484512454542504154510543745";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "9237327312971207141612194050000000000751165014757563737583921083745";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = multiplicar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "39983147599814967196634839748939143313277481566934315383228960569387983023516743292766371658748606370324860152923979429432136698609630925025") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void multiplicar_dois_inteiros_3_test()
{
    const char *cadeia_A = "4328432483243283244711201331232173121632461022484512454542504154510543745";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "0000000000000";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = multiplicar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "0") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void multiplicar_dois_inteiros_4_test()
{
    const char *cadeia_A = "4328432483243283244711201331232173121632461022484512454542504154510543745";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "1";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = multiplicar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "4328432483243283244711201331232173121632461022484512454542504154510543745") == 0);
    assert(resultado_AB.sinal == numero_A.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void multiplicar_dois_inteiros_5_test()
{
    const char *cadeia_A = "85";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "1";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = multiplicar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "85") == 0);
    assert(resultado_AB.sinal == numero_B.sinal);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void multiplicar_dois_inteiros_6_test()
{
    const char *cadeia_A = "934";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "1835";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = multiplicar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "1713890") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void multiplicar_dois_inteiros_7_test()
{
    const char *cadeia_A = "218213832121374";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "182742";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = multiplicar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "39876832109524127508") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void multiplicar_dois_inteiros_8_test()
{
    char cadeia_A[] = "000012";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "23";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = multiplicar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "276") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void multiplicar_dois_inteiros_test()
{
    multiplicar_dois_inteiros_1_test();
    multiplicar_dois_inteiros_2_test();
    multiplicar_dois_inteiros_3_test();
    multiplicar_dois_inteiros_4_test();
    multiplicar_dois_inteiros_5_test();
    multiplicar_dois_inteiros_6_test();
    multiplicar_dois_inteiros_7_test();
    multiplicar_dois_inteiros_8_test();
}

static void subtrair_dois_inteiros_1_test()
{
    char cadeia_A[] = "100";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "56";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "44") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_2_test()
{
    const char *cadeia_A = "346";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "187";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "159") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_3_test()
{
    const char *cadeia_A = "999";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "444";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "555") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_4_test()
{
    const char *cadeia_A = "346";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "187";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "159") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_5_test()
{
    const char *cadeia_A = "346";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "187";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "159") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_6_test()
{
    const char *cadeia_A = "1";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "7888889";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "7888888") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_7_test()
{
    const char *cadeia_A = "9";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "8";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "1") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_8_test()
{
    const char *cadeia_A = "9";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "8";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "1") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_9_test()
{
    const char *cadeia_A = "8";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "9";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = somar_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "1") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}
static void subtrair_dois_inteiros_10_test()
{
    const char *cadeia_A = "8";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = NEGATIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "9";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = NEGATIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "1") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_11_test()
{
    const char *cadeia_A = "9";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "8";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "1") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_12_test()
{
    const char *cadeia_A = "8";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "9";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "1") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_13_test()
{
    const char *cadeia_A = "0";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "7";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "7") == 0);
    assert(resultado_AB.sinal == NEGATIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_14_test()
{
    const char *cadeia_A = "7";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "0";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "7") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_15_test()
{
    const char *cadeia_A = "423423";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "423423";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "0") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_16_test() {
    const char *cadeia_A = "432432678";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "300092116";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "132340562") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_17_test() {
    const char *cadeia_A = "43243253195719630021075874189580000000000092468736178199011303215222659288933740061";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "132912332103213127312030303120000000381373087120312713201327013263474364304024044";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos,
                  "43110340863616416893763843886459999999618719381615865485809976201959184924629716017") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}

static void subtrair_dois_inteiros_18_test() {
    const char *cadeia_A = "659288933740061";
    size_t tamanho_A = strlen(cadeia_A);
    enum sinal sinal_A = POSITIVO;
    struct inteiro numero_A = criar_inteiro(cadeia_A, sinal_A, tamanho_A);

    const char *cadeia_B = "474364304024044";
    size_t tamanho_B = strlen(cadeia_B);
    enum sinal sinal_B = POSITIVO;
    struct inteiro numero_B = criar_inteiro(cadeia_B, sinal_B, tamanho_B);

    struct inteiro resultado_AB = subtrair_dois_inteiros(&numero_A, &numero_B);
    assert(strcmp(resultado_AB.quociente.algarismos, "184924629716017") == 0);
    assert(resultado_AB.sinal == POSITIVO);

    deletar_inteiro(&numero_A);
    deletar_inteiro(&numero_B);
    deletar_inteiro(&resultado_AB);
}


static void subtrair_dois_inteiros_test()
{
    subtrair_dois_inteiros_1_test();
    subtrair_dois_inteiros_2_test();
    subtrair_dois_inteiros_3_test();
    subtrair_dois_inteiros_4_test();
    subtrair_dois_inteiros_5_test();
    subtrair_dois_inteiros_6_test();
    subtrair_dois_inteiros_7_test();
    subtrair_dois_inteiros_8_test();
    subtrair_dois_inteiros_9_test();
    subtrair_dois_inteiros_10_test();
    subtrair_dois_inteiros_11_test();
    subtrair_dois_inteiros_12_test();
    subtrair_dois_inteiros_13_test();
    subtrair_dois_inteiros_14_test();
    subtrair_dois_inteiros_15_test();
    subtrair_dois_inteiros_16_test();
    subtrair_dois_inteiros_17_test();
    subtrair_dois_inteiros_18_test();
}

int main(int argc, char const *argv[])
{
    criar_inteiro_vazio_test();
    criar_inteiro_test();
    somar_dois_inteiros_test();
    multiplicar_dois_inteiros_test();
    subtrair_dois_inteiros_test();
    return 0;
}
