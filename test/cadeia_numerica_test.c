#include "../src/memoria/cadeia_numerica.h"
#include <assert.h>
#include <string.h>

static void cadeia_numerica_vazia_test()
{
    struct cadeia_numerica cadeia;

    cadeia = criar_cadeia_numerica_vazia();

    assert(cadeia_numerica_vazia(&cadeia) == true);
}

static void cadeia_numerica_test()
{
    const char *cadeia_A = "43282348202348";
    size_t tamanho_cadeia_A = strlen(cadeia_A);
    struct cadeia_numerica cadeia_numerica_A = criar_cadeia_numerica(cadeia_A, tamanho_cadeia_A);
    assert(cadeia_numerica_A.algarismos != NULL);
    assert(strncmp(cadeia_numerica_A.algarismos, cadeia_A, tamanho_cadeia_A) == 0);
    assert(strcmp(cadeia_numerica_A.algarismos, cadeia_A) == 0);
    assert(cadeia_numerica_A.tamanho_cadeia == tamanho_cadeia_A);
    assert(cadeia_numerica_A.tamanho_cadeia_alocada == tamanho_cadeia_A + 1);
    deletar_cadeia_numerica(&cadeia_numerica_A);
}

static void cadeia_numerica_string_validacao_test()
{
    const char *cadeia = "23121382318321";
    const size_t tamanho = strlen(cadeia);
    assert(cadeia_numerica_string_valida(cadeia, tamanho) == true);

    const char *cadeia_vazia = "";
    const size_t tamanho_vazio = strlen(cadeia_vazia);
    assert(cadeia_numerica_string_valida(cadeia_vazia, tamanho_vazio) == false);

    const char *cadeia_invalida = "sad332338r";
    const size_t tamanho_cadeia_invalida = strlen(cadeia_invalida);
    assert(cadeia_numerica_string_valida(cadeia_invalida, tamanho_cadeia_invalida) == false);
}

static void cadeia_numerica_inserir_novo_algarismo_test()
{
    // cadeia nova

    const char *cadeia_A = "43282348202348";
    size_t tamanho_cadeia_A = strlen(cadeia_A);
    struct cadeia_numerica cadeia_numerica_A = criar_cadeia_numerica(cadeia_A, tamanho_cadeia_A);
    size_t tamanho_cadeia_numerica_A_alocada = cadeia_numerica_A.tamanho_cadeia_alocada;

    char algarismo = '9';
    assert(inserir_algarismo_em_cadeia_numerica(&cadeia_numerica_A, algarismo) == cadeia_numerica_A.tamanho_cadeia);
    const char *cadeia_A_final = "432823482023489";
    size_t tamanho_cadeia_A_final = strlen(cadeia_A_final);
    assert(strcmp(cadeia_numerica_A.algarismos, cadeia_A_final) == 0);
    assert(strncmp(cadeia_numerica_A.algarismos, cadeia_A_final, tamanho_cadeia_A_final) == 0);
    assert(cadeia_numerica_A.tamanho_cadeia == tamanho_cadeia_A_final);
    assert(cadeia_numerica_A.tamanho_cadeia_alocada == tamanho_cadeia_numerica_A_alocada * 2);

    const char algarismo_B = '1';
    const char *cadeia_A_FINAL_2 = "4328234820234891";
    size_t tamanho_cadeia_A_final_2 = strlen(cadeia_A_FINAL_2);
    assert(inserir_algarismo_em_cadeia_numerica(&cadeia_numerica_A, algarismo_B) == cadeia_numerica_A.tamanho_cadeia);
    assert(strcmp(cadeia_numerica_A.algarismos, cadeia_A_FINAL_2) == 0);
    assert(strncmp(cadeia_numerica_A.algarismos, cadeia_A_FINAL_2, tamanho_cadeia_A_final_2) == 0);
    assert(cadeia_numerica_A.tamanho_cadeia == tamanho_cadeia_A_final_2);
    assert(cadeia_numerica_A.tamanho_cadeia_alocada == tamanho_cadeia_numerica_A_alocada * 2);

    deletar_cadeia_numerica(&cadeia_numerica_A);

    // -- fim cadeia nova --

    //teste com cadeia_numerica == NULL
    assert(inserir_algarismo_em_cadeia_numerica(NULL, '8') == -1);

    //teste com cadeia vazia
    struct cadeia_numerica cadeia_vazia = criar_cadeia_numerica_vazia();
    assert(inserir_algarismo_em_cadeia_numerica(&cadeia_vazia, '7') == cadeia_vazia.tamanho_cadeia);
    assert(strcmp(cadeia_vazia.algarismos, "7") == 0);
    assert(strncmp(cadeia_vazia.algarismos, "7",1) == 0);
    assert(cadeia_vazia.tamanho_cadeia == 1);
    assert(cadeia_vazia.tamanho_cadeia_alocada == 10);
    deletar_cadeia_numerica(&cadeia_vazia);

    //teste com character não numerico
    const char *cadeia_character = "23484328342342932443234432";
    size_t tamanho_cadeia_character = strlen(cadeia_character);
    struct cadeia_numerica cadeia_character_nao_numerico = criar_cadeia_numerica(cadeia_character, tamanho_cadeia_character);
    assert(inserir_algarismo_em_cadeia_numerica(&cadeia_character_nao_numerico, 'a') == -1);
    deletar_cadeia_numerica(&cadeia_character_nao_numerico);
}

static void cadeia_numerica_limpar_cadeia_e_reaproveitar_test()
{

    const char *string = "34284384328432832484328324";
    size_t tamanho = strlen(string);
    struct cadeia_numerica cadeia = criar_cadeia_numerica(string, tamanho);
    const char algarismo = '9';
    size_t tamanho_alocamento = cadeia.tamanho_cadeia_alocada;
    limpar_cadeia_numerica(&cadeia);
    assert(inserir_algarismo_em_cadeia_numerica(&cadeia, algarismo) == cadeia.tamanho_cadeia);
    assert(strcmp(cadeia.algarismos, "9") == 0);
    assert(strncmp(cadeia.algarismos, "9", 1) == 0);
    assert(cadeia.tamanho_cadeia == 1);
    assert(cadeia.tamanho_cadeia_alocada == tamanho_alocamento);

    const char *string_B = "91";
    size_t tamanho_string_B = strlen(string_B);
    const char algarismo_B = '1';
    assert(inserir_algarismo_em_cadeia_numerica(&cadeia, algarismo_B) == cadeia.tamanho_cadeia);
    assert(strcmp(cadeia.algarismos, string_B) == 0);
    assert(strncmp(cadeia.algarismos, string_B, tamanho_string_B) == 0);
    assert(cadeia.tamanho_cadeia == tamanho_string_B);
    assert(cadeia.tamanho_cadeia_alocada == tamanho_alocamento);

    deletar_cadeia_numerica(&cadeia);
}

static void cadeia_numerica_reverter_test()
{
    const char *string_A = "0123456789";
    size_t tamanho = strlen(string_A);
    struct cadeia_numerica cadeia_A = criar_cadeia_numerica(string_A, tamanho);
    const char *string_A_reverter = "9876543210";
    reverter_cadeia_numerica(&cadeia_A);
    assert(strncmp(cadeia_A.algarismos, string_A_reverter, tamanho) == 0);
    assert(strcmp(cadeia_A.algarismos, string_A_reverter) == 0);
    deletar_cadeia_numerica(&cadeia_A);

    const char *string_B = "1";
    size_t tamanho_B = strlen(string_B);
    struct cadeia_numerica cadeia_B = criar_cadeia_numerica(string_B, tamanho_B);
    const char *string_B_reverter = "1";
    reverter_cadeia_numerica(&cadeia_B);
    assert(strncmp(cadeia_B.algarismos, string_B_reverter, tamanho_B) == 0);
    assert(strcmp(cadeia_B.algarismos, string_B_reverter) == 0);
    deletar_cadeia_numerica(&cadeia_B);

    const char *string_C = "123";
    size_t tamanho_c = strlen(string_C);
    struct cadeia_numerica cadeia_C = criar_cadeia_numerica(string_C, tamanho_c);
    const char *string_C_reverter = "321";
    reverter_cadeia_numerica(&cadeia_C);
    assert(strncmp(cadeia_C.algarismos, string_C_reverter, tamanho_c) == 0);
    assert(strcmp(cadeia_C.algarismos, string_C_reverter) == 0);
    deletar_cadeia_numerica(&cadeia_C);

    const char *string_D = "98";
    size_t tamanho_D = strlen(string_D);
    struct cadeia_numerica cadeia_D = criar_cadeia_numerica(string_D, tamanho_D);
    const char *string_D_reverter = "89";
    reverter_cadeia_numerica(&cadeia_D);
    assert(strncmp(cadeia_D.algarismos, string_D_reverter, tamanho_D) == 0);
    assert(strcmp(cadeia_D.algarismos, string_D_reverter) == 0);
    deletar_cadeia_numerica(&cadeia_D);

    const char *string_E = "113282928213912744012032782450108428176129810186";
    size_t tamanho_E = strlen(string_E);
    struct cadeia_numerica cadeia_E = criar_cadeia_numerica(string_E, tamanho_E);
    const char *string_E_reverter = "681018921671824801054287230210447219312829282311";
    reverter_cadeia_numerica(&cadeia_E);
    assert(strncmp(cadeia_E.algarismos, string_E_reverter, tamanho_E) == 0);
    assert(strcmp(cadeia_E.algarismos, string_E_reverter) == 0);
    deletar_cadeia_numerica(&cadeia_E);
}

static void cadeia_numerica_comparacao_test()
{
    // teste de FALHA
    struct cadeia_numerica cadeia_A = criar_cadeia_numerica_vazia();
    assert(comparar_cadeias_numericas(NULL, &cadeia_A) == FALHA);
    assert(comparar_cadeias_numericas(&cadeia_A, NULL) == FALHA);
    assert(comparar_cadeias_numericas(&cadeia_A, &cadeia_A) == FALHA);
    const char *string_A = "3284432843282";
    size_t tamanho_A = strlen(string_A);
    struct cadeia_numerica cadeia_B = criar_cadeia_numerica(string_A, tamanho_A);
    assert(comparar_cadeias_numericas(&cadeia_B, &cadeia_A) == FALHA);
    deletar_cadeia_numerica(&cadeia_B);
    deletar_cadeia_numerica(&cadeia_A);
    // -- teste de FALHA -- //

    // teste string maior | menor | igual
    const char *string_MAIOR = "234943432432";
    size_t tamanho_MAIOR = strlen(string_MAIOR);
    struct cadeia_numerica cadeia_MAIOR_A = criar_cadeia_numerica(string_MAIOR, tamanho_MAIOR);

    const char *string_MENOR = "192323";
    size_t tamanho_MENOR = strlen(string_MENOR);
    struct cadeia_numerica cadeia_MENOR_A = criar_cadeia_numerica(string_MENOR, tamanho_MENOR);

    assert(comparar_cadeias_numericas(&cadeia_MAIOR_A, &cadeia_MENOR_A) == MAIOR);
    assert(comparar_cadeias_numericas(&cadeia_MENOR_A, &cadeia_MAIOR_A) == MENOR);
    assert(comparar_cadeias_numericas(&cadeia_MENOR_A, &cadeia_MENOR_A) == IGUAL);
    assert(comparar_cadeias_numericas(&cadeia_MAIOR_A, &cadeia_MAIOR_A) == IGUAL);

    deletar_cadeia_numerica(&cadeia_MAIOR_A);
    deletar_cadeia_numerica(&cadeia_MENOR_A);
    // -- teste string maior | menor | igual -- //

    // teste cadeia numerica com tamanho igual
    const char *string_tamanho_igual_valor_maior = "925678935";
    size_t tamanho_igual_valor_maior = strlen(string_tamanho_igual_valor_maior);
    struct cadeia_numerica cadeia_tamanho_igual_valor_maior = criar_cadeia_numerica(string_tamanho_igual_valor_maior, tamanho_igual_valor_maior);

    const char *string_tamanho_igual_valor_menor = "456783210";
    size_t tamanho_igual_valor_menor = strlen(string_tamanho_igual_valor_maior);
    struct cadeia_numerica cadeia_tamanho_igual_valor_menor = criar_cadeia_numerica(string_tamanho_igual_valor_menor, tamanho_igual_valor_menor);

    assert(comparar_cadeias_numericas(&cadeia_tamanho_igual_valor_maior, &cadeia_tamanho_igual_valor_menor) == MAIOR);
    assert(comparar_cadeias_numericas(&cadeia_tamanho_igual_valor_menor, &cadeia_tamanho_igual_valor_maior) == MENOR);
    assert(tamanho_igual_valor_maior == tamanho_igual_valor_menor);
    
    deletar_cadeia_numerica(&cadeia_tamanho_igual_valor_maior);
    deletar_cadeia_numerica(&cadeia_tamanho_igual_valor_menor);
    // -- teste cadeia numerica com tamanho igual -- //

}

int main(int argc, char const *argv[])
{
    cadeia_numerica_vazia_test();
    cadeia_numerica_test();
    cadeia_numerica_string_validacao_test();
    cadeia_numerica_inserir_novo_algarismo_test();
    cadeia_numerica_limpar_cadeia_e_reaproveitar_test();
    cadeia_numerica_reverter_test();
    cadeia_numerica_comparacao_test();
    return 0;
}
